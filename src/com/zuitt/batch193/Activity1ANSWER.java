package com.zuitt.batch193;

import java.util.Scanner;

public class Activity1ANSWER {

    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);

        System.out.println("Input year to be checked if it is a leap year");

        int year = sc.nextInt();

        //A leap year is div by 4, div by 400 but no div by 100
        if (year % 4 == 0 && year % 100 != 0)
            System.out.println(year + " is a leap year");
        else if (year % 400 == 0)
            System.out.println(year + " is a leap year");
        else
            System.out.println(year + " is NOT a leap year");
    }
}

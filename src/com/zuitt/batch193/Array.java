package com.zuitt.batch193;

public class Array {

    public static void main(String[] args){

        int[] primeNumber = new int[5];

        primeNumber[0] = 2;
        primeNumber[1] = 3;
        primeNumber[2] = 5;
        primeNumber[3] = 7;
        primeNumber[4] = 11;

        System.out.println("The first prime number is: " + primeNumber[0]);
        System.out.println("The second prime number is: " + primeNumber[1]);
        System.out.println("The third prime number is: " + primeNumber[2]);
        System.out.println("The fourth prime number is: " + primeNumber[3]);
        System.out.println("The fifth prime number is: " + primeNumber[4]);






    }
}
